QT += quick sensors charts network multimedia sql
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS MULTIMEDIA

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    _vk_auth.cpp \
    cryptocontroller.cpp \
    db_controller.cpp \
    tcpcontroller.cpp \
    messagecontroller.cpp

RESOURCES += qml.qrc


INCLUDEPATH += "C:\OpenSSL-Win32\include"

win32-g++ {

LIBS += "C:/OpenSSL-Win32/lib/libeay32.lib" \
        "C:/OpenSSL-Win32/lib/ssleay32.lib" \
        "C:/OpenSSL-Win32/lib/ubsec.lib"

}
#Виды подключения библиотек
# 1) Библиотека с исходными кодами ( *.h + *.cpp)
# Плюсы: Простота подключения
# Минусы: Такие библиотеки подходят только для небольшого количества кода + перестраиваются каждый раз при запуске приложения

# 2) Статическая библиотека (еденичный файл *.lib (windows) \ *.a (unix))
# плюсы: строится отдельно 1 раз
# минусы: сложность подключения, бинарная несовместимость, в каждом приложении нужна своя копия, объем, невозможно обновить версию библиотеки без обн. версии.

# 3) Динамические библиотеки (*.dll, *.so)
# Плюсы: строится отдельно от программы, не требует отдельных копий
# Минусы: Сложность подключения, бинарная несовместимость

# 4) Статическая привязка динамической библиотеки
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    _vk_auth.h \
    cryptocontroller.h \
    db_controller.h \
    tcpcontroller.h \
    messagecontroller.h
