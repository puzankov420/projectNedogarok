#include "_vk_auth.h"


FriendsObject::FriendsObject(const QString &FriendId, const QString &FriendName, const QString &Photo)
    :   ob_friendid(FriendId),
        ob_friendname(FriendName),
        ob_photo(Photo)
{

}


QString FriendsObject::FriendId() const {
    return ob_friendid;
}

QString FriendsObject::FriendName() const {
    return ob_friendname;
}

QString FriendsObject::Photo() const {
    return ob_photo;
}


_vk_auth::_vk_auth(QObject *parent) : QObject(parent)
{

}

void _vk_auth::GetFriends()
{
    int pos1, pos2;
    QEventLoop loop;
    connect(na_manager, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));

    request.setUrl("https://api.vk.com/method/friends.get?user_ids=" + client_id + "&fields=bdate&access_token=" + access_token + "&v=5.74");
    reply = na_manager->get(request);
    loop.exec();
    QString friends = reply->readAll();
    QString name, surname;


    pos1 = 0;
    pos2 = 0;

    while (friends.indexOf("\"id\"", pos2) != -1) {
        pos1 = friends.indexOf("\"id\"", pos2) + 5;
        pos2 = friends.indexOf(",", pos1);
        friends_id.append(friends.mid(pos1, pos2 - pos1));

        pos1 = friends.indexOf("\"first_name\"", pos2) + 14;
        pos2 = friends.indexOf("\"", pos1);
        name = friends.mid(pos1, pos2 - pos1);

        pos1 = friends.indexOf("\"last_name\"", pos2) + 13;
        pos2 = friends.indexOf("\"", pos1);
        surname = friends.mid(pos1, pos2 - pos1);

        friends_names.append(name + " " + surname);
    }


    for (int i = 0; i < friends_id.size(); i++) {
        pos1 = 0;
        pos2 = 0;
        request.setUrl("https://api.vk.com/method/users.get?user_ids=" + friends_id[i] + "&fields=photo_id&access_token=" + access_token + "&v=5.74");
        reply = na_manager->get(request);
        loop.exec();
        response = reply->readAll();
        pos1 = response.indexOf("\"photo_id\"") + 12;
        pos2 = response.indexOf("\"", pos1);
        request.setUrl("https://api.vk.com/method/photos.getById?photos=" + response.mid(pos1, pos2- pos1) + "&access_token=" + access_token + "&v=5.74");
        reply = na_manager->get(request);
        loop.exec();
        response = reply->readAll();
        pos1 = response.indexOf("\"photo_75\"") + 12;
        pos2 = response.indexOf("\"", pos1);
        photos.append(response.mid(pos1, pos2 - pos1).replace("\\", ""));
        QThread::msleep(600);
    }

    for (int i = 0; i < friends_id.size(); i++) {
        qDebug() << photos[i];
        friendsModel.addFriend(FriendsObject(friends_id[i], friends_names[i], photos[i]));
    }
}


void _vk_auth::AuthVK(QString login, QString password) {
    QString clientId = "6469726";
    friends_id.clear();
    friends_names.clear();
    photos.clear();
    request.setUrl("https://oauth.vk.com/authorize" //имя метода
                    "?client_id=" + clientId //id нашего приложения
                    + "&display=page" //веб-старницы будут в мобильном стиле
                    "&redirect_uri=https://oauth.vk.com/blank.html" //адрес куда вы хотите перенаправляться, blank.html это значит никуда не хотите перенаправляться
                    "&scope=friends" //название ресурса которое запрашивается, в данно случае друзья (битовая маска настроек доступа приложения)
                    "&response_type=token" //что хотим в ответ, здесь это token
                    "&v=5.37" //версия API VK
                    "&state=123456");
    QEventLoop loop;
    connect(na_manager, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));
    reply = na_manager->get(request);

    loop.exec();
    response = reply->readAll();
    QString origin, to, ip_h, lg_h;
    QString findStr;
    int pos1, pos2;

    //Поиск ORIGIN
    findStr = "name=\"_origin\" value=\"";
    pos1 = response.indexOf(findStr) + findStr.length();
    findStr = '\"';
    pos2 = response.indexOf(findStr, pos1);
    origin = response.mid(pos1, pos2 - pos1);


    //Поиск ip_h
    findStr = "name=\"ip_h\" value=\"";
    pos1 = response.indexOf(findStr) + findStr.length();
    findStr = '\"';
    pos2 = response.indexOf(findStr, pos1);
    ip_h = response.mid(pos1, pos2 - pos1);


    //Поиск to
    findStr = "name=\"to\" value=\"";
    pos1 = response.indexOf(findStr) + findStr.length();
    findStr = '\"';
    pos2 = response.indexOf(findStr, pos1);
    to = response.mid(pos1, pos2 - pos1);


    //Поиск lg_h
    findStr = "name=\"lg_h\" value=\"";
    pos1 = response.indexOf(findStr) + findStr.length();
    findStr = '\"';
    pos2 = response.indexOf(findStr, pos1);
    lg_h = response.mid(pos1, pos2 - pos1);



    QString authrequest = "https://login.vk.com/"
    "?act=login"
    "&soft=1"
    "&utf8=1"
    "&_origin=" + origin +
    "&lg_h=" + lg_h +
    "&ip_h=" + ip_h +
    "&to=" + to +
    "&email=" + login +
    "&pass=" + QUrl::toPercentEncoding(password);
    qDebug() << authrequest;
    request.setUrl(authrequest);

    reply = na_manager->get(request);
    loop.exec();
    response = reply->header(QNetworkRequest::LocationHeader).toString();

    reply = na_manager->get(QNetworkRequest(QUrl(reply->header(QNetworkRequest::LocationHeader).toString())));
    loop.exec();
    response = reply->header(QNetworkRequest::LocationHeader).toString();

    reply = na_manager->get(QNetworkRequest(QUrl(reply->header(QNetworkRequest::LocationHeader).toString())));
    loop.exec();
    response = reply->readAll();

    response = reply->header(QNetworkRequest::LocationHeader).toString();
    access_token = response.mid(response.indexOf("access_token=") + 13, response.indexOf("&expires_in") - response.indexOf("access_token=") - 13);
    client_id = response.mid(response.indexOf("user_id=") + 8, response.indexOf("&state=") - response.indexOf("user_id=") - 8);


    GetFriends();
}




FriendsModel::FriendsModel(QObject *parent)
{

}

void FriendsModel::addFriend(const FriendsObject &newFriend)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    ob_friends << newFriend;
    endInsertRows();
}


int FriendsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ob_friends.count();

}


void FriendsModel::clearModel()
{
    beginRemoveRows(QModelIndex(), 0, rowCount());
    endRemoveRows();
    ob_friends.clear();
}

QVariant FriendsModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= ob_friends.count())
            return QVariant();
    const FriendsObject &itemToReturn = ob_friends[index.row()];
    if (role == FriendIdRole)
        return itemToReturn.FriendId();
    else if (role == FriendNameRole)
        return itemToReturn.FriendName();
    else if (role == PhotoRole)
        return itemToReturn.Photo();

    return QVariant();


}


QVariantMap FriendsModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

QHash<int, QByteArray> FriendsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[FriendIdRole] = "friendid";
    roles[FriendNameRole] = "friendname";
    roles[PhotoRole] = "photo";
    return roles;

}




