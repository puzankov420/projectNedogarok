#ifndef _VK_AUTH_H
#define _VK_AUTH_H

#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QEventLoop>
#include <QStringList>
#include <QAbstractListModel>
#include <QDesktopServices>
#include <QThread>


class FriendsObject {
    public:
        FriendsObject (const QString &FriendId,
                       const QString &FriendName,
                       const QString &Photo);

        QString FriendId() const;
        QString FriendName() const;
        QString Photo() const;
    private:
        QString ob_friendid;
        QString ob_friendname;
        QString ob_photo;
};


class FriendsModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum DataRoles {
        FriendIdRole,
        FriendNameRole,
        PhotoRole
    };

    FriendsModel(QObject *parent = 0);

    void addFriend(const FriendsObject & newFriend);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    Q_INVOKABLE void clearModel();
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<FriendsObject> ob_friends;

};


class _vk_auth : public QObject
{
    Q_OBJECT
public:
    FriendsModel friendsModel;
    explicit _vk_auth(QObject *parent = nullptr);
     QNetworkAccessManager * na_manager = new QNetworkAccessManager(this);
     QNetworkReply * reply;
     QNetworkRequest request;
     QString access_token;
     QString client_id;
     QString response;
     void GetFriends();
     QStringList friends_id;
     QStringList friends_names;
     QStringList photos;
signals:

public slots:
    void AuthVK (QString login, QString password);
};

#endif // _VK_AUTH_H
