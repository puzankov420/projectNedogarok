#include "db_controller.h"
#include <QSqlDatabase>
#include <QSqlQuery>// класс инкапсулирующий : (инкапсулирует интферфейс для связи с СУБД)
// строку sql запроса
// сигналы и слоты готовности СУБД
// результат, возвращаемый СУБД
// код ошибок
// прочий второстепенный функционал

db_controller::db_controller(QObject *parent) : QObject(parent)
{

    db = QSqlDatabase::addDatabase("QSQLITE"); // тип бд

    db.setDatabaseName("fts.sqlite"); // имя файла бд к которому будет привязан этот объект БД

    db.open();


}



void db_controller::sendQuieries()
{
    QSqlQuery query("CREATE TABLE friends"
                    "(userID int ,"
                    "userName varchar(255));");
    query.exec();


    db.close();

}

db_controller::~db_controller()
{
    db.close();
}
