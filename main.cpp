#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <_vk_auth.h>
#include <cryptocontroller.h>
#include <QQmlContext>
#include "db_controller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    _vk_auth vk_auth;
    QQmlApplicationEngine engine;
    QQmlContext * ctxt = engine.rootContext();

    ctxt->setContextProperty("modelFriends", &(vk_auth.friendsModel));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;


    QObject * appWindow = engine.rootObjects().first();


    db_controller database;
    database.sendQuieries();

    Cryptocontroller cryptocontroller;


    cryptocontroller.encryptFile("f0.txt", "123456789", "123456789");
    cryptocontroller.DencryptFile("f1.txt", "123456789", "123456789");

    QObject::connect(appWindow,
        SIGNAL(auth(QString, QString)),
        &vk_auth,
        SLOT(AuthVK(QString,QString)));
    return app.exec();
}

/*
четыре типа связи:

1. Видимость QML-объекьа из С++
можно вызвать только после загрузки движка и QML-файла
ссылка на QML-окно - обладатель сигнала

2.  2-й ТИП СВЯЗЫВАНИЯ: СВЯЗЬ QML-СИГНАЛА С C++-СЛОТОМ можно вызывать ТОЛЬКО после загрузки движка,
QML-файла и объекта - обладателя слота

3. 3-й ТИП СВЯЗЫВАНИЯ: ВИДИМОСТЬ С++ ОБЪЕКТА В QML

4. 4-й ТИП СВЯЗЫВАНИЯ: РЕАКЦИЯ QML НА С++ СИГНАЛЫ
 1. Объявить кастомный сигнал в классе в разделе signals:
 2. Вызвать emit имя_сигнала();
 3. С помощью 3-го способа сделать испускающий объект видимым в QML
 4. С помощью Connections в QML запустить JavaScript-код для сигнала onИмя_сигнала
*/
