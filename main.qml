import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import QtGraphicalEffects 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Tabs")
    signal auth(string login, string password)

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        Page{
            id: authPage
            ColumnLayout {
                anchors.fill: parent;

                Rectangle {
                    Layout.preferredWidth:  40 * Screen.pixelDensity
                    Layout.preferredHeight: 30 * Screen.pixelDensity
                    Layout.alignment: Qt.AlignCenter

                    ColumnLayout {
                        anchors.fill: parent;
                        Label {
                            font.pointSize: 14
                            font.family: "TimesNewRoman"
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: "Авторизация в ВК"
                        }

                        Rectangle {
                            border.width: 1
                            border.color: "black"
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 8 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            TextField {
                                id: login
                                font.pointSize: 8
                                anchors.fill: parent;
                                placeholderText: "Enter your password"
                                //при размещении в лэйоут элементов,  мы не используем width и x,y
                            }
                        }
                        Rectangle {
                            border.width: 1
                            border.color: "black"
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 8 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            TextField {
                                id: password
                                echoMode: "Password"
                                font.pointSize: 8
                                anchors.fill: parent;
                                placeholderText: "Enter your password"
                                //при размещении в лэйоут элементов,  мы не используем width и x,y
                            }
                        }


                        Button {
                            id: authBtn
                            text: "Commit"
                            font.pointSize: 18
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 10 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter

                            onClicked: {
                                auth(login.text, password.text)
                                password.clear()
                                login.clear()
                            }

                        }
                        DropShadow {
                            anchors.fill: authBtn
                            horizontalOffset: 3
                            verticalOffset: 3
                            radius: 8.0
                            samples: 17
                            color: "#80000000"
                            source: authBtn

                        }
                    }
                }
            }
        }

        Page{
            id: photoPage
            z: 1
            MediaPlayer {
                id: mediaPlayer
                autoLoad: false
                loops: MediaPlayer.Infinite
                source: ""
                autoPlay: false

            }

            Camera {
                id: camera
            }

            ColumnLayout {
                anchors.fill: parent
                VideoOutput {
                    enabled: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    source: camera
                }
                ColumnLayout {
                    Layout.preferredWidth:  40 * Screen.pixelDensity
                    Layout.preferredHeight: 30 * Screen.pixelDensity
                    Layout.alignment: Qt.AlignCenter
                    Button {
                        text: "Сделать\nфото"
                        onClicked:  {
                            camera.imageCapture.captureToLocation("/screenshots")
                            camera.imageCapture.capture();
                        }

                    }
                    Button {
                        text: "Сделать\nвидео"
                    }
                }


            }

        }

        Page { //мультимедия
            MediaPlayer{
                //класс для открытия и обработки аудио/видеофайлов
                id: mediaplayer
                autoLoad: true //грузиться файл сразу, без команды
                loops: MediaPlayer.Infinite //число повторений
                autoPlay: true

                source: "file:///C:/Qt/projects/ProjectNed/sample.avi"
                //source: "sample.avi"
            }
            ColumnLayout{
                          anchors.fill: parent
                          Button{
                                             text: "stop"
                                             font.pointSize: 18
                                             anchors.right: parent.right
                                             onClicked: {mediaplayer.pause()}}
                                         Button{
                                             text: "play"
                                             font.pointSize: 18
                                             anchors.top: parent.top
                                             onClicked: {mediaplayer.play()}}
                          VideoOutput{
                              enabled: true
                              Layout.fillWidth: true
                              Layout.fillHeight: true
                              //anchors.file: parent
                              source: mediaplayer

                          }
        }
        }
        Page {
            id: vkFriends
            Rectangle {
                anchors.fill: parent
                color: "lightblue"
            }

            Item {
                id: item1
                anchors.fill: parent
                   ListView {
                       anchors.fill: parent
                       model: modelFriends
                       spacing: 20
                       delegate: Rectangle {
                               id: rec1
                               color: "white"
                               height: 95
                               width: parent.width
                               radius: 10
                               anchors.margins: 20
                               opacity: 0.8

                               GridLayout {
                                   anchors.fill: parent
                                   opacity: 1
                                   Image {
                                       id: avatar
                                       source: photo
                                       anchors.right: parent.right
                                       anchors.top: parent.top
                                       anchors.rightMargin: 25
                                       anchors.topMargin: 10
                                       Layout.row: 1
                                       Layout.column: 1
                                       Layout.rowSpan: 1
                                   }

                                   Text {
                                       id: id1
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.left
                                       anchors.leftMargin: 10
                                       Layout.row: 1
                                       Layout.column: 2
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "ID друга - " + friendid
                                   }
                                   Text {
                                       id: id2
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.left
                                       anchors.leftMargin: 10
                                       Layout.row: 0
                                       Layout.column: 2
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "Имя друга: " + friendname
                                   }
                               }
                           }
                   }
            }
        }



    }




    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
    }
}
