#include "messagecontroller.h"
#include <QTcpServer>
#include <QTcpSocket>

/*
 * tcpserver слот сервера на входящие соединения, слот который должен создавать новый сокет соединения,
 * у сокетов должны быть слоты, которые реагируют на поступлении информации
 * сигнал сервера NewConnection
 * слоты сокетов connect ready Read
 *
 *
*/
messagecontroller::messagecontroller(QObject *parent) : QObject(parent)
{

serv = new QTcpServer();
client_sock = new QTcpSocket();

QObject::connect(serv, SIGNAL(newConnection()),
                this, SLOT(newConnection()));

  serv->listen(QHostAddress("127.0.0.1"), 33333);
  QObject::connect(client_sock, SIGNAL(readyRead()),
                   this, SLOT(clientReady()));
  client_sock->connectToHost("127.0.0.1",33333);
    QTcpServer serv;
    QTcpSocket sock;
    serv.listen(QHostAddress::LocalHost, 33333);
}


void messagecontroller::newConnection()
{
    server_sock = serv->nextPendingConnection();
    connect(server_sock, SIGNAL(readyRead()),
                     this, SLOT(server_sock_ready()));
}

void messagecontroller::server_sock_ready()
{
    // если сообщение короткое и уместилось в одну посылку -
    // можно считать через client_sock->readAll()
    // если массив (>1Кб) данных передаётся по частям
    // то 1) нужно каким-то образом (например по текстовым меткам)
    // отслеживать начало и конец сообщения
    // 2) считывать фрагменты через client_sock->read()
    // 3) возможно также будет необходимо накапливать все
    // фрагменты в одном буфере
}

void messagecontroller::client_socket_ready()
{

}


void messagecontroller::sendMessage()
{

}
