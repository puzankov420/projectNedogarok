#ifndef MESSAGECONTROLLER_H
#define MESSAGECONTROLLER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class messagecontroller : public QObject
{
    Q_OBJECT
public:
    explicit messagecontroller(QObject *parent = nullptr);
    QTcpServer * serv;
    QTcpSocket * client_sock; // сокет имитирующий соединение
    QTcpSocket * server_sock;
signals:

public slots:

    void newConnection();
    void server_sock_ready();
    void client_socket_ready();
    void sendMessage();
};

#endif // MESSAGECONTROLLER_H
